__ToDo_App__
__The necessary conditions:__
- established docker-compose
***
__Download repository__

[ToDo_App](https://gitlab.com/Schwarzer_Schlosser/todo_app)

***
__Edit File .env.example__

`SK_KEY=django-insecure-ex@b^su8j--8fdr4ccj5koojx1mq1euy!ke4_$+c18c=2-td2(`

`MAIL_HOST_USER=Your email on ukr.net`

`MAIL_HOST_PASSWORD=Your password ukr.net`

`DEF_FROM_EMAIL=Your email on ukr.net`

`MAIL_HOST=smtp.ukr.net`

***
__Deploying the project to docker__

`docker-compose up --build`
***
__Create a super user__

`docker-compose run app python manage.py createsuperuser`

***
__To generate a coverage report coverage__

`docker-compose run app coverage run manage.py test -v 2`

`docker-compose run app coverage html`
***
__Starting development server__

[http://0.0.0.0:8000/](http://0.0.0.0:8000/)

