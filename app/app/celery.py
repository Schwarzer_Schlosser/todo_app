import os
from celery import Celery
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'app.settings')

app = Celery('app')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()


app.conf.beat_schedule = {
    'send_email_every_day': {
        'task': 'todo.tasks.send_everyday_email',
        'schedule': crontab(minute=0, hour=8),
    },
    'send_email_every_week': {
        'task': 'todo.tasks.send_every_week_email',
        'schedule': crontab(day_of_week=6, hour=9, minute=0),
    }
}
