from django.views import View
from django.views.generic.edit import UpdateView
from users.forms import UserCreateForm, MyAuthenticationForm, UserUpdateProfileForm
from django.contrib.auth.views import LoginView
from django.shortcuts import render, redirect
from .tasks import send_email_for_verify_celery
from django.core.exceptions import ValidationError

from django.contrib.auth import authenticate, login
from django.urls import reverse_lazy
from django.utils.http import urlsafe_base64_decode
from django.contrib.auth.tokens import default_token_generator as token_generator
from .models import User

from django.contrib.sites.shortcuts import get_current_site


class MyLoginView(LoginView):
    form_class = MyAuthenticationForm


class EmailVerify(View):

    def get(self, request, uidb64, token):
        user = self.get_user(uidb64)

        if user is not None and token_generator.check_token(user, token):
            user.email_verify = True
            user.save()
            login(request, user)
            return redirect('todo_list')
        return redirect('invalid_verify')

    @staticmethod
    def get_user(uidb64):
        try:
            # urlsafe_base64_decode() decodes to bytestring
            uid = urlsafe_base64_decode(uidb64).decode()
            user = User.objects.get(pk=uid)
        except (
            TypeError,
            ValueError,
            OverflowError,
            User.DoesNotExist,
            ValidationError,
        ):
            user = None
        return user


class MyRegister(View):
    template_name = 'registration/register.html'

    def get(self, request):
        context = {
            'form': UserCreateForm()
        }
        return render(request, self.template_name, context)

    def post(self, request):
        form = UserCreateForm(request.POST)

        if form.is_valid():
            form.save()
            email = form.cleaned_data.get('email')
            password = form.cleaned_data.get('password1')
            user = authenticate(email=email, password=password)
            current_site = get_current_site(request).domain
            send_email_for_verify_celery.delay(user.email, current_site)
            return redirect('confirm_email')
        context = {
            'form': form
        }
        return render(request, self.template_name, context)


class UpdateProfile(UpdateView):
    model = User
    form_class = UserUpdateProfileForm
    template_name = 'registration/profile.html'
    success_url = reverse_lazy('todo_list')
