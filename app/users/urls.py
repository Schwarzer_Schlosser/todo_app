from django.urls import path, include
from django.contrib.auth import urls
from .views import MyRegister, EmailVerify, MyLoginView, UpdateProfile
from django.views.generic import TemplateView

urlpatterns = [
    path('login/', MyLoginView.as_view(), name='login'),
    path('update_profile/<int:pk>', UpdateProfile.as_view(), name='update_profile'),
    path('', include('django.contrib.auth.urls')),

    path('invalid_verify/', TemplateView.as_view(
        template_name='registration/invalid_verify.html'),
         name='invalid_verify'),

    path('verify_email/<uidb64>/<token>/',
         EmailVerify.as_view(),
         name='verify_email',),

    path('confirm_email/', TemplateView.as_view(
        template_name='registration/confirm_email.html'),
        name='confirm_email'),

    path('register/', MyRegister.as_view(), name='register'),
]
