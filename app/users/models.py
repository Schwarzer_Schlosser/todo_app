from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext_lazy as _


class User(AbstractUser):
    username = models.CharField(
        _("username"),
        max_length=150,
        unique=False,
        blank=True,
    )
    email = models.EmailField(
        _('email address'),
        unique=True,
        error_messages={
            "unique": _("A user with that username already exists."),
        },)
    job_title = models.CharField(max_length=160, null=True)
    email_verify = models.BooleanField(default=False)

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ["username"]

