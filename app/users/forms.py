from django import forms
from django.core.exceptions import ValidationError
from django.contrib.auth import authenticate
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from .models import User
from django.contrib.sites.shortcuts import get_current_site
from .tasks import send_email_for_verify_celery


class MyAuthenticationForm(AuthenticationForm):

    def clean(self):
        username = self.cleaned_data.get("username")
        password = self.cleaned_data.get("password")

        if username and password:
            self.user_cache = authenticate(
                self.request,
                username=username,
                password=password
            )

            if self.user_cache is None:
                raise self.get_invalid_login_error()

            if not self.user_cache.email_verify:
                current_site = get_current_site(self.request).domain
                send_email_for_verify_celery.delay(self.user_cache.email, current_site)
                raise ValidationError(
                    'Email not verify! check your email',
                    code="invalid_login",
                )

            else:
                self.confirm_login_allowed(self.user_cache)

        return self.cleaned_data


class UserCreateForm(UserCreationForm):

    email = forms.EmailField(label='Email', widget=forms.EmailInput(attrs={'class': 'form-input'}),
                             required=True)
    first_name = forms.CharField(label='Имя', widget=forms.TextInput(attrs={'class': 'form-input'}),
                                 required=True, max_length=50)
    last_name = forms.CharField(label='Фамилия', widget=forms.TextInput(attrs={'class': 'form-input'}),
                                required=True, max_length=50)
    job_title = forms.CharField(label='Должность', widget=forms.TextInput(attrs={'class': 'form-input'}),
                                required=True, max_length=160)
    password1 = forms.CharField(label='Пароль', widget=forms.PasswordInput(attrs={'class': 'form-input'}),
                                required=True)
    password2 = forms.CharField(label='Повтор пароля', widget=forms.PasswordInput(attrs={'class': 'form-input'}),
                                required=True)

    class Meta:
        model = User
        fields = ('email',
                  'first_name',
                  'last_name',
                  'job_title',
                  'password1',
                  'password2')


class UserUpdateProfileForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'job_title']
