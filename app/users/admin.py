from django.contrib import admin
from .models import User

from django.contrib.auth.admin import UserAdmin
from users.forms import UserCreationForm, UserUpdateProfileForm
from todo.models import Task


@admin.register(User)
class MyUserAdmin(UserAdmin):
    list_display = ("email", "first_name", "last_name", "job_title", "all_tasks", "count_tas")
    search_fields = ["first_name", "last_name"]
    save_on_top = True
    actions = ["user_activated", "user_deactivated"]

    # readonly_fields = ("first_name", "last_name", "email", "all_tasks", "tasks_in_line")
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": ("email", "job_title", "password1", "password2"),
            },
        ),
    )
    fieldsets = (
        (
            None,
            {
                "fields": ("first_name", "last_name", "email", "all_tasks", "tasks_in_line")
            }
        ),
    )

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ['first_name', 'last_name', 'email', "all_tasks", "tasks_in_line"]
        return []

    def all_tasks(self, obj):
        return Task.count_user(self, user=obj.pk)

    def count_tas(self, obj):
        print(obj.pk, '***', obj.id)

    def tasks_in_line(self, obj):
        return Task.in_line(self, user=obj.pk)

    def user_deactivated(self, request, queryset):

        row_update = queryset.update(is_active=False)
        if row_update == '1':
            message_bit = '1 user banned'
        else:
            message_bit = f'{row_update} users banned'
        self.message_user(request, f'{message_bit}')

    def user_activated(self, request, queryset):

        row_update = queryset.update(is_active=True)
        if row_update == '1':
            message_bit = '1 user activated'
        else:
            message_bit = f'{row_update} users activated'
        self.message_user(request, f'{message_bit}')

    user_activated.short_description = 'Activated'
    user_activated.allowed_permissions = ('change',)

    user_deactivated.short_description = 'Deactivated'
    user_deactivated.allowed_permissions = ('change',)

    add_form = UserCreationForm
    form = UserUpdateProfileForm
