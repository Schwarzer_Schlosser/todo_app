from django.core.mail import send_mail
from app.celery import app
from datetime import date, timedelta
from .models import Task
from app import settings


@app.task
def send_everyday_email():

    tasks = Task.objects.filter(complete=date.today() + timedelta(days=1))
    messages = {}
    for task in tasks:
        if task.user not in messages:
            messages[task.user] = [task.title]
        else:
            messages[task.user].append(task.title)
    if messages:
        for email in messages:
            send_mail(
                subject='Ежедневное оповещение',
                message="Завтра дедлайн таких задач:\n"
                        "-{}".format('\n-'.join(messages[email])),
                from_email=settings.EMAIL_HOST_USER,
                recipient_list=[email],
                fail_silently=False,
            )


@app.task
def send_every_week_email():

    start_date = date.today() - timedelta(days=7)
    end_date = date.today()
    tasks = Task.objects.filter(complete__range=(start_date, end_date))
    messages = {}

    for task in tasks:
        if task.user not in messages:
            messages[task.user] = [task.title]
        else:
            messages[task.user].append(task.title)
    if messages:
        for email in messages:
            send_mail(
                subject='Еженедельное оповещение',
                message="За прошедшую неделю вы выполнили такие задачи:\n"
                        "-{}".format('\n-'.join(messages[email])),
                from_email=settings.EMAIL_HOST_USER,
                recipient_list=[email],
                fail_silently=False,
            )
