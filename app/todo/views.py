from django.shortcuts import render
from django.urls import reverse_lazy
from django.core.paginator import Paginator

from django.views.generic import ListView, View, CreateView, UpdateView, DetailView, DeleteView
from .forms import TaskCreateForm
from .models import Task

from users.models import User

from .serializers import TodoListSerializer, ToDoUpdateSerializer, MyUserSerializer
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import GenericViewSet, ModelViewSet
from rest_framework.pagination import PageNumberPagination
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
from rest_framework.mixins import RetrieveModelMixin, UpdateModelMixin
from datetime import timedelta


class TodoListView(View):
    def get(self, request):
        tasks = Task.objects.filter(user=request.user.id)

        if 'qwerty' in request.GET:
            qwerty = request.GET['qwerty']
            tasks = tasks.filter(title__icontains=qwerty, user=request.user)

        paginator = Paginator(tasks, 3)
        page_number = request.GET.get('page')
        page_obj = paginator.get_page(page_number)

        context = {'tasks': page_obj}
        return render(request, 'todo/todo_list.html', context=context)


class TodoCreateView(CreateView):
    model = Task
    template_name = 'todo/todo_create_form.html'
    form_class = TaskCreateForm
    success_url = reverse_lazy('todo_list')

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(TodoCreateView, self).form_valid(form)


class TodoUpdateViews(UpdateView):
    model = Task
    template_name = 'todo/todo_update_form.html'
    form_class = TaskCreateForm
    success_url = reverse_lazy('todo_list')


class TodoDetailView(DetailView):
    model = Task
    context_object_name = 'task'
    template_name = 'todo/todo_detail.html'


class TodoDeleteView(DeleteView):
    model = Task
    context_object_name = 'task'
    success_url = reverse_lazy('todo_list')


class StatisticListView(ListView):
    model = Task
    template_name = 'todo/task_statistic.html'
    context_object_name = 'statistic'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        datas = context['object_list'].values('create', 'complete')
        total_seconds = sum([(data['complete'] - data['create'].date()).total_seconds() for data in datas])
        summa = str(timedelta(seconds=(round(total_seconds / datas.count()))))

        context['count_all'] = context['object_list'].count()
        context['count_todo'] = context['object_list'].filter(status='todo').count()
        context['count_in_progress'] = context['object_list'].filter(status='in_progress').count()
        context['count_blocked'] = context['object_list'].filter(status='blocked').count()
        context['count_finished'] = context['object_list'].filter(status='finished').count()
        context['midl_time'] = summa

        return context


#  *** DJANGO REST FRAMEWORK ***


class MyToDoViewSetPagination(PageNumberPagination):
    page_size = 3
    page_size_query_param = 'page_size'
    max_page_size = 100


class MyToDoViewSet(ModelViewSet):
    permission_classes = [IsAuthenticated]
    pagination_class = MyToDoViewSetPagination
    filter_backends = [DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter]
    filterset_fields = ['status']
    search_fields = ['title']
    ordering_fields = ['complete']

    def get_queryset(self):
        user = self.request.user
        return Task.objects.filter(user=user)

    def get_serializer_class(self):
        if self.request.method in ['GET', 'POST']:
            return TodoListSerializer
        else:
            return ToDoUpdateSerializer

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class UpdateProfileViewSet(RetrieveModelMixin, UpdateModelMixin, GenericViewSet):
    serializer_class = MyUserSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        user = self.request.user
        return User.objects.filter(pk=user.id)
