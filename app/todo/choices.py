from django.db import models
from django.utils.translation import gettext_lazy as _


class TaskPriorityChoices(models.TextChoices):
    LOW = 'low', _('Low')
    HIGH = 'high', _('High')
    MEDIUM = 'medium', _('Medium')


class TaskStatusChoices(models.TextChoices):
    TODO = 'todo', _('Todo')
    IN_PROGRESS = 'in_progress', _('In progress')
    BLOCKED = 'blocked', _('Blocked')
    FINISHED = 'finished', _('Finished')
