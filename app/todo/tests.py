from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APITestCase
from rest_framework.test import force_authenticate
from users.models import User
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework import status


class TestCaseBase(APITestCase):
    @property
    def bearer_token(self):

        user = User.objects.create_user(username='john',
                                        email='test_email_1@gmail.com',
                                        first_name='Test_first_name_1',
                                        last_name='Test_last_name_1',
                                        job_title='Test_position_1',
                                        password='test_password')

        refresh = RefreshToken.for_user(user)
        return {"HTTP_AUTHORIZATION": f'Bearer {refresh.access_token}'}


class TaskTestCase(TestCaseBase):

    def test_get_list_auth(self):
        url = reverse('my_tasks-list')
        response = self.client.get(url, **self.bearer_token)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_list_no_auth(self):
        url = reverse('my_tasks-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_post_list_auth(self):
        url = reverse('my_tasks-list')
        response = self.client.get(url, **self.bearer_token)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_post_list_no_auth(self):
        url = reverse('my_tasks-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
