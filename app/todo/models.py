from django.db import models
from django.utils import timezone
from users.models import User
from .choices import TaskPriorityChoices, TaskStatusChoices
from django.core.validators import MinValueValidator
from datetime import date


class Task(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    title = models.CharField(max_length=200)
    description = models.TextField(blank=True, null=True)
    priority = models.CharField(choices=TaskPriorityChoices.choices,
                                max_length=20,
                                default=TaskPriorityChoices.MEDIUM
                                )
    importance = models.BooleanField(default=False)
    status = models.CharField(choices=TaskStatusChoices.choices,
                              max_length=20,
                              default=TaskStatusChoices.TODO)
    complete = models.DateField(default=timezone.now, validators=[MinValueValidator(date.today())])
    create = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.title} {self.description}'

    def count_user(self, user):
        return Task.objects.filter(user=user).count()

    def in_line(self, user):
        return Task.objects.filter(user=user).filter(status=TaskStatusChoices.IN_PROGRESS)

    class Meta:
        verbose_name = 'Task'
        verbose_name_plural = 'Tasks'
