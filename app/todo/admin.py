from django.contrib import admin
from .models import Task, User
from django.template.response import TemplateResponse
from django.urls import path
from datetime import date, timedelta


@admin.register(Task)
class TodoAdmin(admin.ModelAdmin):
    list_display = ["title", "description", "user_name"]
    search_fields = ["title"]
    list_filter = ["user"]
    fields = ["title", "description", "user", "status"]
    readonly_fields = ["user"]
    change_list_template = "admin/todo/task/change_list.html"

    def user_name(self, obj):
        if obj.user:
            return f"{obj.user.first_name} {obj.user.last_name}"
        return '-'

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path('dash/', self.dash_stat_view, name='dash')
        ]
        return my_urls + urls

    def dash_stat_view(self, request):
        tasks = Task.objects.all()
        user = User.objects.all()

        start_date = date.today() - timedelta(days=7)
        end_date = date.today()

        context = {
            'all_users': user.count(),
            'reg_user_last_week': user.filter(date_joined__range=(start_date, end_date)).count(),
            'all_tasks': tasks.count(),
            'tasks_todo': tasks.filter(status='todo').count(),
            'tasks_in_progress': tasks.filter(status='in_progress').count(),
            'tasks_blocked': tasks.filter(status='blocked').count(),
            'tasks_finished': tasks.filter(status='finished').count()
        }

        return TemplateResponse(request, "admin/todo/task/dashboard.html", context)
