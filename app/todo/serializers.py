from rest_framework import serializers
from .models import Task
from users.models import User


class TodoListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Task
        fields = ('title', 'description', 'status', 'complete', 'id')
        read_only_fields = ('status', 'id')


class ToDoUpdateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Task
        fields = '__all__'
        read_only_fields = ('id', 'create', 'user')


class MyUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'email', 'first_name', 'last_name', 'job_title')
        read_only_fields = ('id', 'email',)
