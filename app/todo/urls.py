from django.urls import path
from rest_framework import routers
from .views import TodoListView, TodoCreateView, TodoUpdateViews, \
    TodoDetailView, StatisticListView, MyToDoViewSet, UpdateProfileViewSet, TodoDeleteView

router = routers.SimpleRouter()
router.register(r'api/my_tasks', MyToDoViewSet, basename='my_tasks')
router.register(r'api/profile', UpdateProfileViewSet, basename='profile')


urlpatterns = [
    path('', TodoListView.as_view(), name='todo_list'),
    path('create/', TodoCreateView.as_view(), name='create_form'),
    path('statistic/', StatisticListView.as_view(), name='task_statistic'),
    path('detail/<int:pk>/', TodoDetailView.as_view(), name='detail'),
    path('delete/<int:pk>/', TodoDeleteView.as_view(), name='delete_task'),
    path('update/<int:pk>/', TodoUpdateViews.as_view(), name='update_list'),
]
